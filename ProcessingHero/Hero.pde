class Hero {
  PVector location;
  ArrayList<PImage> img;
  PImage pimg;
  int iterator, indeks;
  int amp;

  Hero(PVector l, ArrayList<PImage> images) {
    location = l.copy(); 
    img = images; //nie jestem pewien czy to dobry sposob kopiowania ArrayList
    iterator=0;
    indeks=0;
    amp = 1;
  }

  void walk() {
    indeks=(iterator/10)%2*amp;
    pimg=img.get(indeks);
    image(pimg, location.x-pimg.width/2, location.y-pimg.height/2, pimg.width, pimg.height);
    iterator=(iterator+1);
  }
  
   void show() {
    indeks=2;
    pimg=img.get(indeks);
    image(pimg, location.x-pimg.width/2, location.y-pimg.height/2, pimg.width, pimg.height);
  }

  void goRight() {
    location.add(amp*1, 0);  
    walk();
  }

  void goLeft() {
    location.add(-amp*1, 0); 
    walk();
  }

  void goDown() {
    location.add(0, amp*1);  
    walk();
  }

  void goUp() {
    location.add(0, -amp*1); 
    walk();
  }
  
  void goPeriodicRight() {
    location.add(amp*1, 0);  
    walk();
    location.x = (width+location.x) % width;
    location.y = (height+location.y) % height;
  }

  void goPeriodicLeft() {
    location.add(-amp*1, 0); 
    walk();
    location.x = (width+location.x) % width;
    location.y = (height+location.y) % height;
  }

  void goPeriodicDown() {
    location.add(0, amp*1);  
    walk();
    location.x = (width+location.x) % width;
    location.y = (height+location.y) % height;
  }

  void goPeriodicUp() {
    location.add(0, -amp*1); 
    walk();
    location.x = (width+location.x) % width;
    location.y = (height+location.y) % height;
  }
  
  
  
}