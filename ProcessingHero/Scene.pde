class Spot {

  PVector location;
  int radius, clr;

  Spot() {
    location = new PVector(random(width), random(height));
    radius = int(5+random(30));
    clr = int(180+random(50));
  }

  void display() {
    fill(150, clr, 120);
    noStroke();
    ellipse(location.x, location.y, radius, radius);
  }
}



class Scene {
  ArrayList<Spot> spots;
  PVector origin;

  int n;
  int d;
  int dst;

  Scene(int d) {
    spots = new ArrayList<Spot>();

    n=width/d;
    dst=d;
  }

  void addSpot() {
    spots.add(new Spot());
  }

  void display() {
    background(170, 204, 130);
    for (int i = spots.size()-1; i >= 0; i--) {
      Spot p = spots.get(i);
      p.display();
    }

    for (int i=0; i<n; i++){
      stroke(255);
      line(0, i*dst, width, i*dst);
      line(i*dst, 0, i*dst, height);
      fill(0, 0, 0);
      text(i*dst, i*dst+2, 10);
      if (i>0) {
        text(i*dst, 2, i*dst+12);
      }
    }
  }
}

class Target{
  
  PVector location;
  PImage img;
  
  Target(PVector l, PImage images){
    location = l.copy(); 
    img = images;
  }
  
  void display(){
   image(img, location.x-img.width/30, location.y-img.height/30, img.width/15, img.height/15); 
  }
  
}

class Scenario{
  Hero hro;
  Target trg;
  Message msg;
  float distance;
  int counter;
  float x, y, wdh, hgt;
  float c1, c2, c3, c4, c5, c6, c7, sin, freq, cst;
  
  Scenario(Hero h, Target t, Message m){
    hro = h;
    trg = t;
    msg = m;
    
    cst=3;
    freq = 0.1;
    
    wdh = msg.img1.width/cst;
    hgt = msg.img1.height/cst;
    
    x = msg.location.x-0.5*wdh;
    y = msg.location.y-0.5*hgt;
    
    sin = 0;
    
    counter=0;
  }
  
  
  void play(){
   distance = pow(trg.location.x-hro.location.x, 2)+pow(trg.location.y-hro.location.y, 2);
   if(sqrt(distance)<60){ 
   hro.amp=0;
   fill(255);
   sin = sin(freq*counter);
   image(msg.img1, x-10*sin, y-5*sin, wdh+20*sin, hgt+10*sin);
   counter++;
   }
  
  }
  
}

class Message{
  PVector location;
  String msg;
  PImage img1, img2;
  
  Message(PVector l, String sentence, PImage im1, PImage im2){
    location = l.copy(); 
    msg = sentence;
    img1 = im1;
    img2 = im2;
    
  }
  
  
}