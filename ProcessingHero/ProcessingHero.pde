import processing.sound.*;
SoundFile file;

Scene scena;
Hero rycerz;
Target zamek;
Scenario level1;
Message m;

ArrayList<PImage> im = new ArrayList<PImage>();
PImage img_zamek, img_win, img_lost;

void setup() {

  //fullScreen();
  //frameRate(1);
  size(600, 600);
  im.add(loadImage("data/tonyk_right_up.png"));
  im.add(loadImage("data/tonyk_left_up.png"));
  im.add(loadImage("data/tonyk.png"));
  img_zamek = loadImage("data/mystica-Castle-color.png");
  img_win = loadImage("data/win.png");
  img_lost = loadImage("data/lost.png");
  scena = new Scene(50);
  rycerz = new Hero(new PVector(500, 500), im);
  zamek = new Target(new PVector(100, 100), img_zamek);
  m = new Message(new PVector(width/2, height/2), "ZWYCIESTWO!", img_win, img_lost);
  level1 = new Scenario(rycerz, zamek, m);

  for (int i=0; i<200; i++) {
    scena.addSpot();
  }
  file = new SoundFile(this, "Lobo_Loco_-_03_-_Do_You_Remember_ID_356.mp3");
  file.play();
}